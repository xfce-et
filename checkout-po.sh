#!/bin/sh

# checkout-po.sh: checkout PO files via SVN repository.
#
# Copyright (c) 2005 Jean-Francois Wauthy <pollux@xfce.org>
# Copyright (c) 2005 Daichi Kawahata <daichi@xfce.org>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2 of the
# License ONLY.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details at:
# http://gnu.org/licenses/gpl.html

language="et.po"

packages=`cat PACKAGES`

if [ -z "$1" ]
then
	echo "usage:"
	echo "$0 <password>"
	exit 1
fi

root="https://peetervois:$1@svn.xfce.org/svn/"

for package in $packages
do
  dir=${package}/trunk/po/
  if [ ! -d ${dir} ]
  then
    mkdir -p $dir
  fi
  svn co ${root}${package}/trunk/po/ $dir
  unwanted=`find $dir | grep -v $language | grep -v "\.pot" | grep -v "\.svn" | grep -v LINGUAS | grep -v ChangeLog`
  for a in $unwanted
  do
    if [ -f $a ]
    then
      rm -f $a
    fi
  done
done

exit 0

