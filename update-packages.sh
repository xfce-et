#!/bin/sh

# update-packages.sh: check for the PACKAGES in repository.
#
# Copyright (c) 2008 Peeter Vois <Peeter.Vois@proekspert.ee>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2 of the
# License ONLY.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details at:
# http://gnu.org/licenses/gpl.html

repos="xfce goodies"

if [ -z "$1" ]
then
	echo "usage:"
	echo "$0 <password>"
	exit 1
fi

root="https://peetervois:$1@svn.xfce.org/svn/"

for a in $repos
do
	packages=`svn ls $root$a | sed -e 'si/iig'`
	for b in $packages
	do
	   result=`svn ls $root$a/$b/trunk/po 2> /dev/null | grep "ChangeLog"`
	   if [ "$result" = "ChangeLog" ]
	   then
	   		echo "$a/$b"
	   fi
	done
done
