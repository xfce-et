#!/bin/sh

# checkout-po.sh: checkout PO files via SVN repository.
#
# Copyright (c) 2005 Jean-Francois Wauthy <pollux@xfce.org>
# Copyright (c) 2005 Daichi Kawahata <daichi@xfce.org>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2 of the
# License ONLY.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details at:
# http://gnu.org/licenses/gpl.html

language="et.po"

packages=`cat PACKAGES`
files1=`find . -name ${language} | sed 's!\./!!g'`
files2=`find . -name "*.pot" | sed 's!\./!!g'`
files3=`find . -name "LINGUAS" | sed 's!\./!!g'`
files4=`find . -name "ChangeLog" | sed 's!\./!!g'`


if [ -z "$1" ]
then
        echo "usage:"
        echo "$0 <password>"
        exit 1
fi

root="https://peetervois:$1@svn.xfce.org/svn/"

for file in $files1 $files2 $files3 $files4
do
  echo "*** ${file} ***"
  svn switch --non-interactive ${root}${file} ${file}
done

exit 0

