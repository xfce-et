# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-12-13 11:19+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../plugins/display_plugin/display_plugin.c:170
#, c-format
msgid "Old settings will be restored in %d seconds"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:189
msgid ""
"Display settings have been changed.\n"
"Would you like to keep these settings?"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:193
#, c-format
msgid "Previous settings will be restored in %d seconds"
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/display_plugin/display_plugin.c:381
msgid "Button Label|Display"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:716
msgid "Display Preferences"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:751
msgid "Resolution"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:822
#, c-format
msgid "%dx%d@%d"
msgstr ""

#: ../plugins/display_plugin/display_plugin.c:870
msgid "Gamma correction"
msgstr ""

#. Red
#: ../plugins/display_plugin/display_plugin.c:878
msgid "Red"
msgstr ""

#. Green
#: ../plugins/display_plugin/display_plugin.c:889
msgid "Green"
msgstr ""

#. Blue
#: ../plugins/display_plugin/display_plugin.c:900
msgid "Blue"
msgstr ""

#.
#: ../plugins/display_plugin/display_plugin.c:911
msgid "Sync sliders"
msgstr ""

#: ../plugins/fm_plugin/fm_plugin.c:63
msgid "Failed to open the File Manager Preferences."
msgstr ""

#: ../plugins/fm_plugin/fm_plugin.c:65
msgid ""
"Either the Xfce File Manager was not build with support for D-BUS, or the D-"
"BUS service was not installed properly."
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/fm_plugin/fm_plugin.c:86
msgid "Button Label|File Manager"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:531
msgid "Sticky Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:535
msgid "Enable _Sticky Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:540
msgid ""
"Sticky keys allow you to press key combinations (for example, Alt+F) in "
"sequence rather than simultaneously."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:548
msgid "_Latch mode"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:554
msgid ""
"Pressing a modifier key once makes it sticky, and it stays sticky until a "
"non-modifier key is pressed."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:558
msgid "L_ock mode"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:564
msgid ""
"Quickly pressing a modifier key twice makes it sticky, and it stays sticky "
"until a the modifier key is pressed again."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:567
msgid "Disable Sticky Keys if _two keys pressed together"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:573
msgid ""
"Sticky Keys can be disabled automatically (without using this settings "
"panel) by pressing two keys at the same time."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:579
msgid "Slow Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:583
msgid "Enable Slow _Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:588
msgid ""
"Slow Keys causes key presses not to be accepted unless the key is held down "
"for a certain period of time."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:591
msgid "Slow keys _delay:"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:606
#: ../plugins/keyboard_plugin/keyboard_plugin.c:657
msgid "<span style='italic' size='smaller'>Short</span>"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:618
msgid ""
"A shorter delay closer approximates behavior when Slow Keys is disabled, "
"while a longer delay can help to ignore accidental key presses."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:622
#: ../plugins/keyboard_plugin/keyboard_plugin.c:673
msgid "<span style='italic' size='smaller'>Long</span>"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:630
msgid "Bounce Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:634
msgid "Enable _Bounce Keys"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:639
msgid ""
"Bounce Keys helps avoid repeated key presses by not accepting presses of the "
"same key within a certain period of time."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:642
msgid "D_ebounce delay:"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:669
msgid ""
"A shorter delay closer approximates behavior when Bounce Keys is disabled, "
"while a longer delay can help to ignore accidental repeated key presses."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:707
msgid "Keyboard Preferences"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:724
msgid "Keyboard map"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:749
msgid "Typing Settings"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:753
msgid "Enable key repeat"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:762
msgid "Short"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:769
msgid "Long"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:776
#: ../plugins/keyboard_plugin/keyboard_plugin.c:844
#: ../plugins/mouse_plugin/mouse_plugin.c:514
#: ../plugins/mouse_plugin/mouse_plugin.c:610
msgid "Slow"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:783
#: ../plugins/keyboard_plugin/keyboard_plugin.c:851
#: ../plugins/mouse_plugin/mouse_plugin.c:527
#: ../plugins/mouse_plugin/mouse_plugin.c:624
msgid "Fast"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:790
msgid "Delay:"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:797
#: ../plugins/keyboard_plugin/keyboard_plugin.c:837
msgid "Speed:"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:823
#: ../plugins/mouse_plugin/mouse_plugin.c:641
msgid "Cursor"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:828
msgid "Show blinking"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:869
msgid "Use this entry area to test the settings above."
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:871
msgid "Test area"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:880
msgid "Settings"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:888
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1891
msgid "Shortcuts"
msgstr ""

#: ../plugins/keyboard_plugin/keyboard_plugin.c:899
#: ../plugins/mouse_plugin/mouse_plugin.c:648
msgid "Accessibility"
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/keyboard_plugin/keyboard_plugin.c:983
msgid "Button Label|Keyboard"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:834
msgid "Rename theme"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:849
msgid "New name:"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1028
msgid ""
"Shortcut already in use !\n"
"Are you sure you want to use it ?"
msgstr ""

#. Create dialog
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1108
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1585
msgid "Choose command"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1113
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1596
msgid "Command:"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1143
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1640
msgid "The command doesn't exist or the file is not executable !"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1181
msgid "Set shortcut for command:"
msgstr ""

#. Create dialog
#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1184
msgid "Set shortcut"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1188
msgid "Cancel"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1192
msgid "No shortcut"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1361
msgid "New theme"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1375
msgid "Name:"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1408
msgid "File already exists"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1421
msgid "Filename:"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1495
#, c-format
msgid "Do you really want to delete the '%s' theme ?"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1546
#, c-format
msgid "Do you really want to delete the shorcut entry for the '%s' command ?"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1740
msgid "Select an Application"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1750
msgid "All Files"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1755
msgid "Executable Files"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1770
msgid "Perl Scripts"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1776
msgid "Python Scripts"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1782
msgid "Ruby Scripts"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1788
msgid "Shell Scripts"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1861
msgid "Themes"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1975
msgid "Command"
msgstr ""

#: ../plugins/keyboard_plugin/shortcuts_plugin.c:1982
msgid "Shortcut"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:88
#: ../plugins/mouse_plugin/xfce-mouse-settings.desktop.in.h:1
msgid "Mouse Settings"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:108
msgid ""
"<span weight='bold' size='large'>Cursor settings saved.</span>\n"
"\n"
"Mouse cursor settings may not be applied until you restart Xfce."
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:114
msgid "_Don't show this again"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:143
#: ../plugins/mouse_plugin/mouse-cursor-settings.c:151
#, c-format
msgid "Mouse Settings: Unable to create %s"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:162
#, c-format
msgid ""
"Mouse Settings: Unable to move %s to %s.  Cursor settings may not be "
"reapplied correctly on restart."
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:172
#, c-format
msgid ""
"Mouse Settings: Failed to run xrdb.  Cursor settings may not be applied "
"correctly. (Error was: %s)"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:484
msgid "Cursor theme"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:556
msgid "Cursor Size"
msgstr ""

#: ../plugins/mouse_plugin/mouse-cursor-settings.c:573
msgid "Preview"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:361
msgid "Enable mouse emulation"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:374
msgid "Delay :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:387
msgid "Interval :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:404
msgid "Time to max :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:417
msgid "Max speed:"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:451
msgid "Mouse Preferences"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:466
msgid "Behavior"
msgstr ""

#. button order
#: ../plugins/mouse_plugin/mouse_plugin.c:476
msgid "Button settings"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:484
msgid "Left Handed"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:491
msgid "Right handed"
msgstr ""

#. motion settings
#: ../plugins/mouse_plugin/mouse_plugin.c:499
msgid "Motion settings"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:507
msgid "Acceleration :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:533
#: ../plugins/mouse_plugin/mouse_plugin.c:568
msgid "Threshold :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:540
#: ../plugins/mouse_plugin/mouse_plugin.c:575
msgid "Low"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:553
#: ../plugins/mouse_plugin/mouse_plugin.c:588
msgid "High"
msgstr ""

#. drag and drop
#: ../plugins/mouse_plugin/mouse_plugin.c:560
msgid "Drag and drop"
msgstr ""

#. double click
#: ../plugins/mouse_plugin/mouse_plugin.c:595
msgid "Double click"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:603
msgid "Speed :"
msgstr ""

#: ../plugins/mouse_plugin/mouse_plugin.c:719
msgid "Mouse emulation enabled"
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/mouse_plugin/mouse_plugin.c:819
msgid "Button Label|Mouse"
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/screensaver_plugin/screensaver_plugin.c:51
msgid "Button Label|Screensaver"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:122
msgid "System Default"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:501
msgid "Font Selection Dialog"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:529
msgid "DPI Changed"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:531
msgid "DPI was changed successfully"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:532
msgid ""
"However, you may need to restart your session for the settings to take "
"effect."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:534
msgid "Log Out _Later"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:536
msgid "Log Out _Now"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:543
msgid "Exec Error"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:544
msgid "Failed to run \"xfce4-session-logout\""
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:558
msgid "Custom DPI"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:579
msgid ""
"Enter your display's DPI below.  Numbers that are multiples of 6 usually "
"work best.  The smaller the number, the smaller your fonts will look."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:594
msgid "Custom _DPI:"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:631 ../plugins/ui_plugin/ui_plugin.c:1000
msgid "Other..."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:887
msgid "User Interface Preferences"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:913
msgid "_Theme"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:940
msgid "_Icon Theme"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:974
msgid "Font _DPI:"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1005
msgid "Font"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1012
msgid "Toolbar Style"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1016
msgid "Icons"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1017
msgid "Text"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1018
msgid "Both"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1019
msgid "Both horizontal"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1023
msgid "Editable menu accelerators"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1027
msgid "Menu Accelerators"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1037
msgid "Font Rendering"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1041
msgid "Use anti-aliasing for fonts"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1046
msgid ""
"Antialiasing is an effect that is applied to the edges of characters to make "
"the characters look smoother."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1050
msgid "Use hinting:"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1055
msgid ""
"Hinting is a font-rendering technique that improves the quality of fonts at "
"small sizes and an at low screen resolutions. Select one of the options to "
"specify how to apply hinting."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1065
msgid "Slight"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1066
msgid "Medium"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1067
msgid "Full"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1071
msgid "Use sub-pixel hinting:"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1076
msgid ""
"Select one of the options to specify the subpixel color order for your "
"fonts. Use this option for LCD or flat-screen displays."
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1094
msgid "RGB"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1098
msgid "BGR"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1102
msgid "Vertical RGB"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1106
msgid "Vertical BGR"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1126
msgid "List of available GTK+ themes"
msgstr ""

#. the button label in the xfce-mcs-manager dialog
#: ../plugins/ui_plugin/ui_plugin.c:1186
msgid "Button Label|User interface"
msgstr ""

#: ../plugins/ui_plugin/ui_plugin.c:1404
msgid ""
"You have changed font rendering settings. This change will only affect newly "
"started applications."
msgstr ""

#: ../plugins/display_plugin/xfce-display-settings.desktop.in.h:1
msgid "Display Settings"
msgstr ""

#: ../plugins/display_plugin/xfce-display-settings.desktop.in.h:2
msgid "Xfce 4 Display Settings"
msgstr ""

#: ../plugins/keyboard_plugin/xfce-keyboard-settings.desktop.in.h:1
msgid "Keyboard Settings"
msgstr ""

#: ../plugins/keyboard_plugin/xfce-keyboard-settings.desktop.in.h:2
msgid "Xfce 4 Keyboard Settings"
msgstr ""

#: ../plugins/mouse_plugin/xfce-mouse-settings.desktop.in.h:2
msgid "Xfce 4 Mouse Settings"
msgstr ""

#: ../plugins/ui_plugin/xfce-ui-settings.desktop.in.h:1
msgid "User Interface Settings"
msgstr ""

#: ../plugins/ui_plugin/xfce-ui-settings.desktop.in.h:2
msgid "User Interface Settings (themes and fonts)"
msgstr ""

#: ../plugins/ui_plugin/xfce-ui-settings.desktop.in.h:3
msgid "Xfce 4 User Interface Settings"
msgstr ""
